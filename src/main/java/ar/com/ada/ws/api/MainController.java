package ar.com.ada.ws.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class MainController {

    @Autowired
    private MovieOrSerieServices movieOrSerieServices;

    @GetMapping({ "", "/" }) @CrossOrigin(origins = "*")
    public ResponseEntity<List<MovieOrSerie>> getAllMoviesOrSeries(
            @RequestParam Optional<Integer> year,
            @RequestParam Optional<String> type ) throws Exception {

        List<MovieOrSerie> movieOrSeries = movieOrSerieServices.getAllMovieOrSeries(year, type);

        return ResponseEntity.ok(movieOrSeries);
    }
}
