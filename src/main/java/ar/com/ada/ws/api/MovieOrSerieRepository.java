package ar.com.ada.ws.api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieOrSerieRepository extends JpaRepository<MovieOrSerie, Long> {
}
