package ar.com.ada.ws.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class MovieOrSerieServices {

    @Autowired
    private MovieOrSerieRepository movieOrSerieRepository;

    public List<MovieOrSerie> getAllMovieOrSeries(Optional<Integer> year, Optional<String> type) {
        return movieOrSerieRepository.findAll()
                .stream()
                .filter(getFilterRules(year, type))
                .collect(Collectors.toList());
    }

    private Predicate<MovieOrSerie> getFilterRules(Optional<Integer> yearOpt, Optional<String> typeOpt) {
        List<Predicate<MovieOrSerie>> filterConditions = new ArrayList<>();

        yearOpt.ifPresent(year -> filterConditions.add(mos -> mos.getYear().equals(year)));
        typeOpt.ifPresent(type -> filterConditions.add(mos -> mos.getType().equals(type)));

        return filterConditions.stream().reduce(mos -> true, Predicate::and);
    }
}
