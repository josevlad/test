INSERT INTO MovieOrSerie
    (id, title, year, type, gender, imageUrl)
VALUES
    (1, 'Bad Boys para siempre', 2020, 'pelicula', 'Comedia y accion', 'https://cineenlinea.net/wp-content/uploads/2020/01/Poster-Bad-Boys-3.jpg'),
    (2, 'El Stand de los besos', 2020, 'pelicula', 'Comedia romántica adolescente', 'https://upload.wikimedia.org/wikipedia/en/c/c8/The_Kissing_Booth_2_poster.jpg'),
    (3, 'Joker', 2019, 'pelicula', 'Suspenso y drama psicológico', 'https://pics.filmaffinity.com/Joker-790658206-mmed.jpg'),
    (4, 'It 2', 2019, 'pelicula', 'Terror sobrenatural', 'https://es.web.img3.acsta.net/pictures/19/07/30/09/09/0763744.jpg'),
    (5, 'The Umbrella Academy', 2019, 'serie', 'Accion y Aventura', 'https://pics.filmaffinity.com/The_Umbrella_Academy_Serie_de_TV-422973449-mmed.jpg'),
    (6, 'The Rain', 2018, 'serie', 'Accion', 'https://miro.medium.com/max/640/1*NRtDqM7d-ZEiHUaHNySjRQ.jpeg'),
    (7, 'Dark', 2017, 'serie', 'Ciencia ficción', 'https://www.awesomepostersonline.com/uploads/2018-06-01/b1fb1ea4/f336f3fb80e.jpg'),
    (8, 'Star Wars: The Last Jedi', 2017, 'pelicula', 'Ciencia ficción', 'https://i.pinimg.com/originals/26/a8/ab/26a8ab88a1ae4386ef6c051dbbe8b2ed.jpg'),
    (9, 'Scream', 2015, 'serie', 'Terror', 'https://pics.filmaffinity.com/scream_resurrection-304764663-large.jpg'),
    (10, 'Juego de tronos', 2011, 'serie', 'Drama y fantasía medieval', 'https://www.eltiempo.com/files/image_640_428/uploads/2019/05/23/5ce6e2a1369e8.jpeg'),
    (11, 'El precio del mañana', 2011, 'pelicula', 'Ciencia ficción distópica y suspenso', 'https://i.ytimg.com/vi/PT99JkrtkMo/hqdefault.jpg'),
    (12, 'CSI: Miami', 2002, 'serie', 'Acción', 'https://imagenes.gatotv.com/categorias/programas/posters/csi_miami.jpg'),
    (13, 'Harry Potter y la cámara secreta', 2002, 'pelicula', 'Ciencia ficción', 'https://assets.cinepolisklic.com/images/ae0dacc3e95640ec8a3974efba7fe57d_250X375.jpg'),
    (14, 'Friends', 1994, 'serie', 'Comedia', 'https://pics.filmaffinity.com/Friends_Serie_de_TV-875463197-large.jpg'),
    (15, 'Miami Vice', 1990, 'serie', 'Accion', 'https://static.motor.es/fotos-noticias/2020/04/ferrari-365-gts4-daytona-spyder-miami-vice-corrupcion-en-miami-202066850-1587743709_1.jpg'),
    (16, 'Casablanca', 1942, 'pelicula', 'Drama y romance', 'https://lavozenoffdotnet.files.wordpress.com/2013/03/c00.jpg?w=640')
    ;
